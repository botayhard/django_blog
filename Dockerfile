FROM python:latest

COPY . .
RUN pip install -r requirements.txt
RUN cp wsgi.py Blog && cp asgi.py Blog
